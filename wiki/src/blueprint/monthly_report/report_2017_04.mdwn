[[!meta  title="Tails report for April, 2017"]]
[[!meta  date="DATE"]]

[[!toc ]]

Releases
========

* [[Tails VERSION was released on MONTH DAY|news/version_VERSION]] ([major|minor] release).

* Tails VERSION+1 is [[scheduled for MONTH DAY|contribute/calendar]].

The following changes were introduced in Tails VERSION:

XXX: Copy the "Changes" section of the release notes, and compact a bit:

* Remove lines about software upgrade (that's not Tails itself).
* Remove screenshots.
* Remove "New features" and "Upgrades and changes" headlines.
* Remove line about Changelog.

Code
====

XXX: List important code work that is not covered already by the Release
     section (for example, the changes being worked on for the next version).

Documentation and website
=========================

- Cody Brownstein documented how to [[change the password of an encrypted
  partition|doc/encryption_and_privacy/encrypted_volumes#change]].

- Cody Brownstein documented how to [[delete the files corresponding to a
  persistence feature that has been deselected|doc/first_steps/persistence/configure#deselect]].

- We added instructions on how to [[burn DVDs from macOS *El Capitan* or
  later|install/mac/dvd#burn-dvd]].

User experience
===============

- We collected [[intercept interviews|blueprint/intercept_interviews]]
  of users using Tails world-wide to understand better our users, their needs,
  and what they like or dislike in Tails. We already have seven great story
  from Western Europe, North America, Latin America, and Africa.

Infrastructure
==============

We upgraded some moe of our systems to Debian 9 (Stretch), in order to
help identify remaining issues before it becomes the new Debian
stable release.

We made good progress towards using our Vagrant build system on our
Continuous Integration infrastructure, to make it match what
developers use. This is part of our work on Reproducible ISO Builds.

We are interviewing a candidate to join our system administration team.

XXX: Count the number of tests in /features at the beginning of next month

         git checkout `git rev-list -n 1 --before="June 1" origin/devel`
         git grep --extended-regexp '^\s*Scenario:' -- features/*.feature | wc -l

XXX: Report only if more scenarios have been written and add the diff from the previous month, for example:

       - Our test suite covers SCENARIOS scenarios, DIFF more that in May.

- The mechanism that we designed for the [[distribution and activation of the
  revocation certificate of the Tails signing
  key|doc/about/openpgp_keys/signing_key_revocation]] is now deployed and 23
  people own a share of the revocation certificate. It allows revoking the
  Tails signing key even if very bad things happens to most of the team while
  making it hard for isolated and malicious individuals to revoke the signing
  key when not needed. We still encourage experts to review this mechanism and
  other projects to adopt similar practices.

Funding
=======

We are working towards adding
a [[!tails_ticket 6972 desc="Sponsors page"]] to our website, and are
talking with a couple potential corporate sponsors.

We are still in the process of discussing our proposal with OTF, and
reworking it accordingly.

We were nominated for the
[MIT Media Lab Disobedience Award](https://www.media.mit.edu/disobedience/).

Outreach
========

Past events
-----------

Upcoming events
---------------

On-going discussions
====================

XXX: Link to the thread on <https://mailman.boum.org/pipermail/tails-XXX/>.

Press and testimonials
======================

XXX: Copy content from press/media_appearances_2016.mdwn
     This page is continuously updated by tails-press@boum.org, so if
     it's empty there might be nothing special to report.

Translation
===========

XXX: Add the output of `contribute/l10n_tricks/language_statistics.sh`
XXX: Add the output of (adjust month!):

    git checkout $(git rev-list -n 1 --before="September 1" origin/master) && \
    git submodule update --init && \
    ./wiki/src/contribute/l10n_tricks/language_statistics.sh

Metrics
=======

* Tails has been started more than BOOTS/MONTH times this month. This makes BOOTS/DAY boots a day on average.
* SIGS downloads of the OpenPGP signature of Tails ISO from our website.
* 160 bug reports were received through WhisperBack.

XXX: Ask tails@boum.org for these numbers.
