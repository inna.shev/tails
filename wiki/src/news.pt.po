# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2017-02-09 18:52+0100\n"
"PO-Revision-Date: 2015-07-07 16:19+0200\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid "[[!meta title=\"News\"]]\n"
msgid "[[!meta title=\"News\"]]"
msgstr "[[!meta title=\"Notícias\"]]\n"

#. type: Content of: <ul><li><p>
msgid ""
"Subscribe to the [[amnesia-news mailing list|about/contact#amnesia-news]] to "
"receive the same news by email:"
msgstr ""

#. type: Content of: <ul><li><form>
msgid ""
"<input class=\"text\" name=\"email\" value=\"\"/> <input class=\"button\" "
"type=\"submit\" value=\"Subscribe\"/>"
msgstr ""

#. type: Content of: <ul><li><p>
msgid ""
"Follow us on Twitter <a href=\"https://twitter.com/tails_live\">@Tails_live</"
"a>."
msgstr ""

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
#| "(currentlang() or news/report_2* or news/version_0* or news/test_0* or "
#| "news/test_*-rc?) and tagged(announce)\"\n"
#| "show=\"10\" feeds=\"yes\" feedonly=\"yes\" feedfile=\"emails\"]]\n"
msgid ""
"[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
"(currentlang() or news/report_2* or news/version_0* or news/test_0* or news/"
"test_*-rc?)\" show=\"10\" sort=\"age\"]] [[!inline pages=\"page(news/*) and !"
"news/*/* and !news/discussion and (currentlang() or news/report_2* or news/"
"version_0* or news/test_0* or news/test_*-rc?) and tagged(announce)\" show="
"\"10\" feeds=\"yes\" feedonly=\"yes\" feedfile=\"emails\" sort=\"age\"]]"
msgstr ""
"[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
"(currentlang() or news/report_2* or news/version_0* or news/test_0* or news/"
"test_*-rc?) and tagged(announce)\"\n"
"show=\"10\" feeds=\"yes\" feedonly=\"yes\" feedfile=\"emails\"]]\n"

#~ msgid ""
#~ "[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
#~ "(currentlang() or news/report_2* or news/version_0* or news/test_0* or "
#~ "news/test_*-rc?)\" show=\"10\"]]\n"
#~ msgstr ""
#~ "[[!inline pages=\"page(news/*) and !news/*/* and !news/discussion and "
#~ "(currentlang() or news/report_2* or news/version_0* or news/test_0* or "
#~ "news/test_*-rc?)\" show=\"10\"]]\n"
